<?php
    /**
     * @param $msg
     * @param $type
     * @return void
     */

    define('logPath','/logs/run.log');

    /**
     * @return $ip
     */
    function getClientIp() {
        $ip = $_SERVER['REMOTE_ADDR'];
        return $ip;
    }

    /**
     * @return $_SERVER['HTTP_USER_AGENT']
     */
    function getClientAgent() {
        return $_SERVER['HTTP_USER_AGENT'];
    }

    /**
     * @param $msg
     * @param $type
     * @return void
     */
    function writeToLog($msg,$type) {
        date_default_timezone_set("Europe/Berlin");
        $log = documentRoot.logPath;
        $fp = fopen($log,'a+');
        if($type == 'error' || $type == 'debug' || $type == 'notice' || $type == 'security') {
            $msg = preg_replace( "/\r|\n/", "", $msg);
            fwrite($fp,"[API->".strtoupper($type)." || ".date("Y.m.d")." @ ".date("h:i:s")." || From: ".getClientIp()." || Agent: ".getClientAgent()."] ".$msg."\n");
        }
        fclose($fp);
    }
    
    /**
     * @return string
     */
    function checkLogFile() {
        if(!file_exists(documentRoot.logPath)) {
            $newlog = fopen(documentRoot.logPath, "w");
            $txt = "[API->Notice || ".date("Y.m.d")." @ ".date("h:i:s")." || From: ".getClientIp()."] New Log file created because no run.log was found.\n";
            fwrite($newlog, $txt);
            fclose($newlog);
            return "Created new log";
        }
        return "Log already existing";
    }

    //echo checkLogFile();
?>