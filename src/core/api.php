<?php

    /**
     * @author Jamo <info@heliophobix.com>
     * @license MIT
     * 
     * PHP-API, written by @Jamo, www.heliophobix.com
     * Feel free to re-use, abuse and copy my code - do whatever you want.
     * But keep in mind that I'm not responsible for any kind of damage caused by my code.
     * 
     * Coded with ❤ from Germany
     */

    /**
     * @param $path
     * @param $key
     * @return $value
     */
    function iniGet($path,$key) {
        $ini_array = parse_ini_file($path);
        $value = $ini_array[$key];
        return $value;
    }

    /**
     * @param $path
     * @param $key
     * @return $value
     */
    function iniGetRelative($path,$key) {
        $ini_array = parse_ini_file($path);
        $value = $ini_array[$key];
        return $value;
    }

    // Settings
    /**
     * @FIXME
     * error_reporting($_SERVER['DOCUMENT_ROOT'].iniGetRelative('../config/api-settings.conf.ini','errorReporting'));
     */
    define('documentRoot',$_SERVER['DOCUMENT_ROOT'].iniGetRelative('../config/api-settings.conf.ini','serverRoot'));

    session_start();
    session_destroy();
    require_once 'logging.php';

    if(isset($_GET['token']) && $_GET['token'] != NULL) {
        define('requestMode','private');
        writeToLog('Accessing API in mode: '.requestMode,'security');
    } else if(!isset($_GET['token']) || $_GET['token'] != NULL) {
        define('requestMode','public');
        writeToLog('Accessing API in mode: '.requestMode,'security');
    } else {
        writeToLog('Tried to set invalid request mode (public/private)','error');
    }

    // Define stuff

    // Define API's home database
    define('api_dbUser',iniGet(documentRoot.'config/api-settings.conf.ini','api_dbUser'));
    define('api_dbName',iniGet(documentRoot.'config/api-settings.conf.ini','api_dbName'));
    define('api_dbAddr',iniGet(documentRoot.'config/api-settings.conf.ini','api_dbAddr'));
    define('api_dbPass',iniGet(documentRoot.'config/api-settings.conf.ini','api_dbPass'));
    // Define the database which has to get queried by the API
    define('req_dbUser',iniGet(documentRoot.'config/api-settings.conf.ini','req_dbUser'));
    define('req_dbName',iniGet(documentRoot.'config/api-settings.conf.ini','req_dbName'));
    define('req_dbAddr',iniGet(documentRoot.'config/api-settings.conf.ini','req_dbAddr'));
    define('req_dbPass',iniGet(documentRoot.'config/api-settings.conf.ini','req_dbPass'));

    // db handling functions

    /**
     * @param $SQLhost
     * @param $SQLuser
     * @param $SQLpass
     * @param $SQLdbname
     * @return $conn
     * @return bool
     */
    function dbConnect($SQLhost,$SQLuser,$SQLpass,$SQLdbname) {
        $conn = new mysqli($SQLhost, $SQLuser, $SQLpass, $SQLdbname);
        if ($conn->connect_error) {
            writeToLog('Error while trying to connect to database "'.$SQLdbname.'" on "'.$SQLhost.'". Error: '.$conn->connect_error,'error');
            return false;
        }
        return $conn;
    }

    /**
     * @param $conn
     * @return bool
     */
    function checkDbConnectionstate($conn) {
        if(!isset($conn)) {
            writeToLog('Tried to check db connection. Problem: Given connection does not exist!','debug');
            return false;
        }
        if ($conn->connect_error) {
            writeToLog('Error while trying to talk to database. Error: '.$conn->connect_error,'error');
            return $conn->connect_error;    
        }
        return true;
    }

    /**
     * @param $sql
     * @param $conn
     * @return $result
     * @return bool
     */
    function execSQL($sql,$conn) {
        if(checkDbConnectionstate($conn)) {
            $result = $conn->query($sql);
            return $result;
        }
        return false;
    }


    // Check database connections

    $_SESSION['requestDatabaseConnection'] = dbConnect(req_dbAddr,req_dbUser,req_dbPass,req_dbName);
    $_SESSION['apiDatabaseConnection'] = dbConnect(api_dbAddr,api_dbUser,api_dbPass,api_dbName);
    
    $checkApiDb = checkDbConnectionstate($_SESSION['apiDatabaseConnection']);
    $checkRequestDb = checkDbConnectionstate($_SESSION['requestDatabaseConnection']);
    if(!$checkApiDb) {
        writeToLog('Checked connection to request database, got Error: '.$checkRequestDb,'error');
        die();
    } else if(!$checkApiDb) {
        writeToLog('Checked connection to api database, got Error: '.$checkApiDb,'error');
        die();
    } else if($checkApiDb != false && $checkRequestDb != false) {
        writeToLog('Successfully connected to both databases!','debug');
    }

    // Check Token if a token was provided

    /**
     * @return bool
     */
    function checkToken() {
        $requestToken = $_GET['token'];
        $tokenTestResult = execSQL("SELECT * FROM api_tokens WHERE api_token = '$requestToken' AND is_active = 1",$_SESSION['apiDatabaseConnection']);
        if($tokenTestResult->num_rows == 1) {
            writeToLog('Client uses valid token!','debug');
            // Store token Level in session
            $tokenLevelResult = execSQL("SELECT token_level FROM api_tokens WHERE api_token = '$requestToken' AND is_active = 1",$_SESSION['apiDatabaseConnection']);
            while($row = $tokenLevelResult->fetch_assoc()) {
                $_SESSION['apiTokenLevel'] = $row['token_level'];
            }
            return true;
        } else if($tokenTestResult->num_rows == 0) {
            writeToLog('Client tried to connect with invalid token: '.$requestToken,'security');
            echo 'Invalid credentials!';
            http_response_code(401);
            return false;
        } else if($tokenTestResult->num_rows > 1) {
            echo 'Internal error!';
            http_response_code(500);
            writeToLog('There are multiple tokens with the same token string! Internal Error!','error');
            return false;
        } else {
            echo 'Internal error!';
            writeToLog('Token error!','error');
            http_response_code(500);
            return false;
        }
    }

    if(requestMode === 'private') {
        if(!checkToken()) {
            writeToLog('Closing connection.','debug');
            die();
        }
    }

    /**
     * Main function for all requests
     * @param $sql
     * @return $json
     */
    function performDatabaseRequest($suppliedRequestName) {

        // Get all request names
        $jsonRequests = json_decode(file_get_contents('./requests.json'),true);
        $requestNames = [];
        foreach($jsonRequests['requests'] as $requestName) {
            array_push($requestNames,$requestName['name']);
        }

        // Check if $requestName is valid
        if(!in_array($suppliedRequestName,$requestNames)) {
            http_response_code(400);
            echo 'The request you asked for does not exist!';
            writeToLog('User tried to use not existing request: '.$suppliedRequestName,'debug');
            die();
        }

        /**
         * @todo
         *  - Check API Token level
         *  - Get SQL Syntax by name
         *  - Execute db request IF api level matches
         */

        function getRequestArrayByName($name) {
            $jsonRequests = json_decode(file_get_contents('./requests.json'),true);
            foreach($jsonRequests['requests'] as $requestArray) {
                if($requestArray['name'] === $name) {
                    return $requestArray;
                }
            }
        }

        $currentRequestArray = getRequestArrayByName($suppliedRequestName);

        if($currentRequestArray['requiredApiTokenLevel'] <= $_SESSION['apiTokenLevel']) {
            writeToLog('The clients API token is strong enough for the request','debug');
        } else {
            writeToLog('The clients API token is too week for the request!','security');
            echo "Your token is not strong enough!";
            http_response_code(401);
            die();
        }

        function performFinalRequest($sql) {
            $res = execSQL($sql,$_SESSION['requestDatabaseConnection']);
            $rows = array();
            while($r = mysqli_fetch_assoc($res)) {
                $rows[] = $r;
            }
            writeToLog('User performed request: '.$sql,'debug');
            $json =  json_encode($rows);
            header('Content-type: application/json');
            return $json;
        }

        return performFinalRequest($currentRequestArray['sql']);
    }

    echo performDatabaseRequest($_GET['request']);

?>