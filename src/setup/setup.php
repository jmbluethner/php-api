<?php

    function iniGet($path,$key) {
        $path = $_SERVER['DOCUMENT_ROOT'].$path;
        $ini_array = parse_ini_file($path);
        $value = $ini_array[$key];
        return $value;
    }
    function iniGetRelative($path,$key) {
        $ini_array = parse_ini_file($path);
        $value = $ini_array[$key];
        return $value;
    }

    define('documentRoot',iniGetRelative('../config/api-settings.conf.ini','serverRoot'));

    define('api_setup_dbUser',iniGet(documentRoot.'/config/api-settings.conf.ini','api_dbUser'));
    define('api_setup_dbName',iniGet(documentRoot.'/config/api-settings.conf.ini','api_dbName'));
    define('api_setup_dbAddr',iniGet(documentRoot.'/config/api-settings.conf.ini','api_dbAddr'));
    define('api_setup_dbPass',iniGet(documentRoot.'/config/api-settings.conf.ini','api_dbPass'));

    $cfg = './db-models.conf.ini';
    $conn = new mysqli(api_setup_dbAddr, api_setup_dbUser, api_setup_dbPass, api_setup_dbName);

    $table_sql = parse_ini_file($cfg);
    $table_names = array_keys($table_sql);
    $sql_constructors = [];
    foreach($table_sql as $sql) {
        array_push($sql_constructors,$sql);
    }
    $i = 0;
    foreach($table_names as $name) {
        $conn->query($sql_constructors[$i]);
        $i++;
    }

    echo 'Looped through the following tables:<br>';
    foreach($table_names as $table) {
        echo $table.'<br>';
    }
?>
